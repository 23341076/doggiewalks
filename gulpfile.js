var gulp = require('gulp');

var sass = require('gulp-sass');

gulp.task('sass', function() {
  return gulp.src('assets/scss/**/*.scss') //gets all files ending with .scss in app/sccs.
  .pipe(sass())
  .pipe(gulp.dest('app/css'))
});

gulp.task('watch', function() {
  gulp.watch('assets/scss/**/*.scss', gulp.series('sass'));
})
