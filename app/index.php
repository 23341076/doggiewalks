<!DOCTYPE html>
<html>
  <!--Sets the class grid for the page layout-->
  <head class="grid">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DoggieWalks Home</title>

    <!-- Links the stylesheet for the design -->
    <link rel="stylesheet" href="/css/style.css">

  <head>
  <body>
    <header>
      <div class="branding">
        <a href="/">
          <img src="/images/branding/logo.svg"/>
        </a>
      </div>
      <nav class="main-nav">
        <a href="#" class="main-button">
          <i class="fa fa-bars"></i>
        </a>
        <ul>
          <li><a href="contact.php">Contact</a></li>
          <li><a href="gallery.php">Gallery</a></li>
          <li><a href="servicescost.php">Services & Cost</a></li>
          <li><a href="meetteam.php">Meet the team</a></li>
          <li><a href="about.php">About</a></li>
          <li><a class="active" href="index.php">Home</a></li>
        </ul>
      </nav>
    </header>
  </body>

</html>
